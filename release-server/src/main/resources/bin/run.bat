@echo off

set CURRENT_DIR=%cd%
if "%AMDATU_HOME%" == "" (
  echo AMDATU_HOME not set using pwd..
  set AMDATU_HOME=%CURRENT_DIR%
)

set WORK_DIR=%AMDATU_HOME%\work
set PLATFORM_DIR=%AMDATU_HOME%\servers
set DEMO_DIR=%AMDATU_HOME%\demos
set DEPLOY_DIR=%AMDATU_HOME%\deploy

if not exist "%DEPLOY_DIR%" (
 mkdir %DEPLOY_DIR%
)

set platformName=minimal

:loop
if "%1"=="" goto continue
if "%1"=="-h" goto showUsage
if "%1"=="-c" goto doClean
if "%1"=="-p" goto setPlatform
shift
goto loop

:showUsage
echo run [ options]
echo -h show this help message
echo -c clean cache before start
echo -p select a platform (default is minimum)
goto eof

:doClean
if exist "%WORK_DIR%" (
  rd /Q /S %WORK_DIR%
)
shift
goto loop

:setPlatform
shift
if "%1"=="" goto usage
if not exist "%PLATFORM_DIR%\%1" (
  echo Platform not found: %1
  goto eof
)
set platformName=%1
shift
goto loop

:continue
set JAVA_OPTS=%JAVA_OPTS% -Dfile.encoding=utf-8
set JAVA_OPTS=%JAVA_OPTS% -Xms256m -Xmx1024m -XX:MaxPermSize=256m
set JAVA_OPTS=-Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n
set JAVA_OPTS=%JAVA_OPTS% -Dfelix.config.properties=file:conf/amdatu-platform.properties 
set JAVA_OPTS=%JAVA_OPTS% -Dfelix.fileinstall.dir=%PLATFORM_DIR%\%platformName%,%DEPLOY_DIR%

echo Starting Amdatu server (%platformName%)
"%JAVA_HOME%\bin\java" %JAVA_OPTS% -jar lib/org.apache.felix.main-4.0.2.jar

:eof